## Parcours formation Développeur Web - Openclassrooms

Projet 5 - 90 heures

# Construisez un site e-commerce

Compétences évaluées :
- Créer un plan de test pour une application
- Interagir avec un web service avec JavaScript
- Valider des données issues de sources externes
- Gérer des événements JavaScript

# Lancement de l'application !!!

- 1 Cloner le projet
- 2 Ouvrir le projet dans VSCode
- 3 Sélectionner le fichier index.html
- 4 Lancer Live Server
