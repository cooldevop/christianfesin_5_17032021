"use strict";

import Home from "./views/pages/Home.js";
import Listing from "./views/pages/Listing.js";
import ListingShowByCategory from "./views/pages/ListingShowByCategory.js";
import ListingShowByCategoryTeddies from "./views/pages/ListingShowByCategoryTeddies.js";
import About from "./views/pages/About.js";
import Error404 from "./views/pages/Error404.js";
import PostShow from "./views/pages/PostShow.js";
import ProductShow from "./views/pages/ProductShow.js";
import Register from "./views/pages/Register.js";

// import Navbar from "./views/components/Navbar.js";
// import Bottombar from "./views/components/Bottombar.js";

import Utils from "./services/Utils.js";
import {
  initCart,
  handleCartActionButtons,
  checkout,
  getCartId,
} from "./services/Cart.js";
import Checkout from "./views/pages/checkout.js";

// Liste des chemins pris en charge. Toute URL autre que ces routes générera une erreur 404
const routes = {
  // "/": Home,
  "/": ListingShowByCategoryTeddies,
  "/listing": Listing,
  "/about": About,
  "/p/:id": PostShow,
  "/produit/:id": ProductShow,
  "/produit/:id/cameras": ProductShow,
  "/produit/:id/teddies": ProductShow,
  "/produit/:id/furniture": ProductShow,
  "/listing/:id/cameras": ListingShowByCategory,
  "/listing/:id/teddies": ListingShowByCategory,
  "/listing/:id/furniture": ListingShowByCategory,
  "/register": Register,
  "/checkout": Checkout,
};

// Chargement des header et footer:
Utils.loadViewsElements();
// Utils.sleep(5000);
initCart();
handleCartActionButtons();

// Le code du routeur. Prend une URL, vérifie la liste des routes prises en charge, puis affiche la page de contenu correspondante.
const router = async () => {
  // Initialiser l'élément principale à hydrater:
  const content = null || document.getElementById("page_container");

  // Get the parsed URl from the addressbar
  let request = Utils.parseRequestURL();

  // Analyse de l'URL et si elle a une partie identifiant, la modifier avec la chaîne ":id"
  let parsedURL =
    (request.resource ? "/" + request.resource : "/") +
    (request.id ? "/:id" : "") +
    (request.verb ? "/" + request.verb : "");

  // Définir la page de notre hachage des routes prises en charge.
  // Si l'URL analysée ne figure pas dans notre liste de routes prises en charge, sélectionnez la page 404 à la place
  let page = routes[parsedURL] ? routes[parsedURL] : Error404;
  content.innerHTML = await page.render();
  await page.after_render();
};

// Écouter le changement de hash dans l'URL:
window.addEventListener("hashchange", router);

// Ecouter le chargement complet de la page:
window.addEventListener("load", router);
