import Utils from "./../../services/Utils.js";
import { initProduit, handleAddProductButton } from "../../services/Cart.js";

// import styles from "./product.module.css";
// import styles from "./product.module.js";

const heading_style = {
  color: "blue",
};

const productOptionLabel = {
  teddies: "couleur",
  furniture: "vernis",
  cameras: "lentilles",
};

const productOptionName = {
  teddies: "colors",
  furniture: "varnish",
  cameras: "lenses",
};

// ------------------------
// Récupère les informations spécifiques à un produit.
// ------------------------
let getProduct = async (_id, cat) => {
  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const response = await fetch(
      // `http://localhost:3000/api/teddies/` + _id,
      `http://localhost:3000/api/` + cat + "/" + _id,
      options
    );
    const json = await response.json();
    // console.log(json);
    return json;
  } catch (err) {
    console.log("Error getting documents", err);
  }
};

let ProductShow = {
  render: async () => {
    let request = Utils.parseRequestURL();
    let product = await getProduct(request.id, request.verb);
    let optionLabel = productOptionLabel[request.verb];
    let optionName = productOptionName[request.verb];

    let listeOptions = product[optionName];
    // let listeOptions = product;
    // console.log("Liste options", product[optionName]);
    // console.log("Liste options", listeOptions);
    // console.log("Liste options #2: ", product.varnish);

    return /*html*/ `
 
            <section class="section">
              <h2 class="section__title">Ours en peluche</h2>
              <div class="product only">
                <div class="product__image only">
                  <img src="${product.imageUrl}" alt="Ours ${
      product.name
    } " title="${product.name}">
                </div>
                <h2 class="product__name">${product.name}</h2>
                <div class="product__option">
                  <form>
                    <label for="options_produits">Choisir une option de: ${optionLabel}&nbsp; </label>
                    <select id="options_produits" name="options_produits">
                      ${listeOptions
                        .map(
                          (the_option) => /*html*/ `
			                      <option value="${the_option}">${the_option}</option>`
                        )
                        .join("\n ")}
                    </select>
                    <input id="prod_id" type="hidden" name="_id" value="${
                      product._id
                    }"
                    >
                  </form>
                </div>
                <h3 class="product__price">${product.price}</h3>
                <button class="btn btn--primary product__btn" data-action="ADD_TO_CART">Ajouter au panier</button>
              </div>
              <div class="product-only-nav">
                <button class="btn btn btn--primary btn--pay" data-action="CHECKOUT_PRODUCT" disabled>&#10004;payer</button>
                <a href="/#/"><div class="btn btn--primary btn--contact">&#10094; Retourner à la liste des produits</div></a>
              </div>
            </section>
        `;
  },
  after_render: async () => {
    initProduit();
    let request = Utils.parseRequestURL();
    let product = await getProduct(request.id, request.verb);
    handleAddProductButton(product);
  },
};

export default ProductShow;
