// import Cart from "../../services/Cart.js";

let Home = {
  render: async () => {
    // let products = await getProductsList();
    let view = /*html*/ `
            <section class="section">
                <h2 class="section__title">Produits</h2>
                <div class="products">
                  <div class="product">
                    <div class="product__image">
                      <a href="#/listing/categories/teddies"><img src="/img/ourse-en-peluche.jpg" alt="Ourses en peluche"></a>
                    </div>
                    <h2 class="product__name">Ourses en peluche faits main</h2>
                  </div>
                
                  <div class="product">
                    <div class="product__image">
                      <a href="#/listing/categories/cameras"><img src="/img/cameras-vintage.jpg" alt="Caméras vintage"></a>
                    </div>
                    <h2 class="product__name">Caméras vintage</h2>
                  </div>

                  <div class="product">
                    <div class="product__image">
                      <a href="#/listing/categories/furniture"><img src="/img/ameublement.jpg" alt="Ameublement"></a>
                    </div>
                    <h2 class="product__name">Ameublement</h2>
                  </div>

                </div>
            </section>
        `;
    return view;
  },
  after_render: async () => {
    // Cart.initCart();
  },
};

export default Home;
