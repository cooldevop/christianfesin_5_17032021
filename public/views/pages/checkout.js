import { checkout, getCart } from "./../../services/Cart.js";
// let products = getCart();

let Checkout = {
  render: async () => {
    let products = getCart();
    let view = /*html*/ `
        <div class="checkout">
            <section class="section flex">
                <h2 class="section__title">Passer Commande</h2>
                <div>
                    <form id="formCheckout">
                        <ul class="wrapper">
                            <li class="form-row">
                                <label for="firstName">Prénom: *</label>
                                <input type="text" id="firstName" name="firstName" aria-label="Quel est votre prénom" placeholder="Saisissez votre prénom" required pattern="[A-z\ \-]{2,25}" minlength="2" maxlength="25">
                            </li>
                            <li class="form-row">
                                <label for="lastName">Nom: *</label>
                                <input type="text" id="lastName" name="lastName" aria-label="Quel est votre nom" placeholder="Saisissez votre nom" required pattern="[A-z\ \-]{2,25}" minlength="2" maxlength="25">
                            </li>
                            <li class="form-row">
                                <label for="address">Addresse: *</label>
                                <input type="text" id="address" name="address" aria-label="Quel est votre adresse" placeholder="adresse" required pattern="[A-z0-9\ \-]{5,25}" minlength="5" maxlength="25">
                            </li>
                            <li class="form-row">
                                <label for="city">Ville: *</label>
                                <input type="text" id="city" name="city" aria-label="Quel est votre ville de résidence" placeholder="ville de résidence" required pattern="[A-z\ \-]{2,25}" minlength="2" maxlength="25">
            
                            </li>
                            <li class="form-row">
                                <label for="email">Email: *</label>
                                <input type="email" id="email" name="email" aria-label="Quel est votre adresse email" placeholder="email" required>
                            </li>
                            <li class="form-row">
                                <button type="submit" aria-label="Envoyer">Envoyer</button>
                            </li>
                        </ul>
                    </form>
                </div>
            </section>
            <section class="section flex">
                <h2 class="section__title">Produits sélectionnés</h2>
                <div class="cart">
                    ${products
                      .map(
                        (product) => /*html*/ `
                            <div class="cart__item cart-confirm">
                                <img class="cart__item__image" src="${product.image}" alt="${product.name}">
                                <h3 class="cart__item__name">${product.name}</h3>
                                <h3 class="cart__item__price">${product.price}</h3>
                                <h3 class="cart__item__quantity">Quantité:&nbsp;${product.quantity}</h3>
                            </div>`
                      )
                      .join("\n ")}
                </div>

            </section>
        </div>
        `;
    return view;
  },
  after_render: async () => {
    const formCheckout = document.getElementById("formCheckout");
    formCheckout.addEventListener("submit", function (event) {
      checkout(event);
    });
  },
};

export default Checkout;
