// --------------------------------
//  Define Data Sources
// --------------------------------

// import Utils from "../../services/Utils.js";
// import {} from "../../services/Cart.js";

let getProductsList = async () => {
  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const response = await fetch(`http://localhost:3000/api/teddies`, options);
    const json = await response.json();
    // console.log(json)
    return json;
  } catch (err) {
    console.log("Error getting documents", err);
  }
};

let Listing = {
  render: async () => {
    let products = await getProductsList();
    let view = /*html*/ `
            <section class="section">
                <h2 class="section__title">Produits</h2>
                <div class="products">
                    ${products
                      .map(
                        (product) => /*html*/ `
                          <div class="product">
                            <div class="product__image">
                              <a href="#/produit/${product._id}"><img src="${product.imageUrl}" alt="Hoodie"></a>
                            </div>
                            <h2 class="product__name">${product.name}</h2>
                            <h3 class="product__price">${product.price}</h3>
                            <button class="btn btn--primary product__btn" data-action="ADD_TO_CART">Ajouter au panier</button>
                          </div>`
                      )
                      .join("\n ")}
                  </div>
            </section>
        `;
    return view;
  },
  after_render: async () => {
    // Cart.initCart();
  },
};

export default Listing;
