import Utils from "../../services/Utils.js";

// ------------------------
// Récupère la liste des produits depuis le serveur via une requête asynchrone.
// ------------------------
let getProductsList = async (cat) => {
  const options = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  };
  try {
    const response = await fetch(`http://localhost:3000/api/` + cat, options);
    const json = await response.json();
    // console.log(json)
    return json;
  } catch (err) {
    console.log("Error getting documents", err);
  }
};

let Listing = {
  render: async () => {
    let request = Utils.parseRequestURL();
    let products = await getProductsList("teddies");
    let view = /*html*/ `
            <section class="section">
                <h2 class="section__title">Ours en peluche faits à la main</h2>
                <div class="products">
                    ${products
                      .map(
                        (product) => /*html*/ `
                          <div class="product">
                            <div class="product__image">
                              <a href="#/produit/${product._id}/teddies"><img src="${product.imageUrl}" alt="Ours ${product.name} " title="${product.name}"></a>
                            </div>
                            <h2 class="product__name">${product.name}</h2>
                            <h3 class="product__price">${product.price}</h3>
                          </div>`
                      )
                      .join("\n ")}
                  </div>
            </section>
        `;
    return view;
  },
  after_render: async () => {},
};

export default Listing;
