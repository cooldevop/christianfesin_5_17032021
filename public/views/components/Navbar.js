let Navbar = {
  render: async () => {
    let view = /*html*/ `
        header
            <div class="cart-btn">
                <i id="cart" class="fas fa-shopping-cart"></i>
                <span class="cart-quantity">0</span>
            </div>
        `;
    return view;
  },
  after_render: async () => {},
};

export default Navbar;
