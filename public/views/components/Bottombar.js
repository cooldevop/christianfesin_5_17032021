let Bottombar = {
  render: async () => {
    let view = /*html*/ `
<div class="l-footer">
<h1>
Orinoco</h1>
<p>
Ours en peluche confectionnés à la main de manière artisanale</p>
</div>
<ul class="r-footer">
<li class="features">
  <h2 class="information">
Information</h2>
<ul class="box h-box">
<li><a href="#">Blog</a></li>
<li><a href="#">Nos prix</a></li>
<li><a href="#">Revendeurs</a></li>
<li><a href="#">Certifications</a></li>
<li><a href="#">Service client</a></li>
</ul>
</li>
</ul>
<div class="b-footer">
<p>
Tous droits réservés par Orinoco 2021 </p>
</div>
        `;
    return view;
  },
  after_render: async () => {},
};

export default Bottombar;
