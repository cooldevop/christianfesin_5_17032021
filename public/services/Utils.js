import Navbar from "../views/components/Navbar.js";
import Bottombar from "../views/components/Bottombar.js";

const Utils = {
  // --------------------------------
  //  Analyser une URL et la diviser en ressource, identifiant et verbe
  // --------------------------------
  parseRequestURL: () => {
    let url = location.hash.slice(1).toLowerCase() || "/";
    let r = url.split("/");
    let request = {
      resource: null,
      id: null,
      verb: null,
    };
    request.resource = r[1];
    request.id = r[2];
    request.verb = r[3];

    return request;
  },

  // --------------------------------
  //  Implémentation d'une temporisation simple
  // --------------------------------
  sleep: (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  },

  // --------------------------------
  // Lazy load des vues secondaires et Gestion de l'affichage du Panier:
  // --------------------------------
  loadViewsElements: async () => {
    // const header = null || document.getElementById("header_container");
    // header.innerHTML = await Navbar.render();
    // await Navbar.after_render();

    const footer = null || document.getElementById("footer_container");
    footer.innerHTML = await Bottombar.render();
    await Bottombar.after_render();

    // Afficher le Panier
    const cartIcone = document.querySelector("#cart");
    const cartModalOverlay = document.querySelector(".cart-modal-overlay");

    cartIcone.addEventListener("click", () => {
      if (cartModalOverlay.style.transform === "translateX(0)") {
        cartModalOverlay.style.transform = "translateX(-200%)";
      } else {
        cartModalOverlay.style.transform = "translateX(0)";
      }
    });

    // Fermer le Panier
    const closeBtn = document.querySelector("#close-btn");

    closeBtn.addEventListener("click", () => {
      cartModalOverlay.style.transform = "translateX(-200%)";
    });

    cartModalOverlay.addEventListener("click", (e) => {
      if (e.target.classList.contains("cart-modal-overlay")) {
        cartModalOverlay.style.transform = "translateX(-200%)";
      }
    });
  },
  // Afficher le panier
  showCart: () => {
    const cartModalOverlay = document.querySelector(".cart-modal-overlay");

    if (cartModalOverlay.style.transform === "translateX(0)") {
      cartModalOverlay.style.transform = "translateX(-200%)";
    } else {
      cartModalOverlay.style.transform = "translateX(0)";
    }
  },
  // Fermer le panier
  closeCart: () => {
    const cartModalOverlay = document.querySelector(".cart-modal-overlay");
    cartModalOverlay.style.transform = "translateX(-200%)";
  },
};

export default Utils;
