import Utils from "./Utils.js";
// ------------------------
// Initialisation du Panier
// ------------------------

let cart = JSON.parse(localStorage.getItem("cart")) || [];
const cartDOM = document.querySelector(".cart");
const addToCartButtonsDOM = document.querySelectorAll(
  '[data-action="ADD_TO_CART"]'
);
const clearCartButtonDOM = document.querySelector('[data-action="CLEAR_CART"]');
const payButtonDOM = document.querySelector('[data-action="CHECKOUT"]');
const cartFooterTotal = document.querySelector(".cart-footer__total");

clearCartButtonDOM.addEventListener("click", () => clearCart());
payButtonDOM.addEventListener("click", () => goCheckoutForm());

// ------------------------
// Affichage des produits enregistrés dans le panier depuis localStorage.
// ------------------------
function initCart() {
  if (cart.length > 0) {
    cart.forEach((cartItem) => {
      const product = cartItem;
      insertItemToDOM(product);
      countCartTotal();
    });
  }
}

// ------------------------
// Gère l’initialisation d’un produit lors de son insertion dans le DOM.
// ------------------------
function initProduit() {
  const addProductForInit = document.querySelectorAll(
    '[data-action="ADD_TO_CART"]'
  );
  addProductForInit.forEach((addToCartButtonDOM) => {
    addToCartButtonDOM.addEventListener("click", () => {
      const productDOM = addToCartButtonDOM.parentNode;
      const product = {
        image: productDOM
          .querySelector(".product__image img")
          .getAttribute("src"),
        name: productDOM.querySelector(".product__name").innerText,
        price: productDOM.querySelector(".product__price").innerText,
        quantity: 1,
        _id: document.getElementById("prod_id").value,
      };
      const isInCart =
        cart.filter((cartItem) => cartItem.name === product.name).length > 0;

      if (!isInCart) {
        insertItemToDOM(product);
        cart.push(product);
        saveCart();
      }
      cartDOM.innerHTML = "";
      initCart();
      handleCartActionButtons();
      // console.log("product", product);
      handleAddProductButton(product);
    });
  });
}

// ------------------------
// Ajoute un produit dans l’UI du panier.
// ------------------------
function insertItemToDOM(product) {
  cartDOM.insertAdjacentHTML(
    "beforeend",
    `
        <div class="cart__item">
            <img class="cart__item__image" src="${product.image}" alt="${
      product.name
    }">
            <h3 class="cart__item__name">${product.name}</h3>
            <h3 class="cart__item__price">${product.price}</h3>
            <button class="btn btn--primary cart__btn" data-action="DECREASE_ITEM" ${
              product.quantity === 1 ? "disabled" : ""
            }>&#9866;</button>
            <h3 class="cart__item__quantity">${product.quantity}</h3>
            <button class="btn btn--primary cart__btn" data-action="INCREASE_ITEM">&#10010;</button>
            <button class="btn btn--danger cart__btn" data-action="REMOVE_ITEM">&#10006;</button>
        </div>
    `
  );
  clearCartButtonDOM.disabled = false;
  payButtonDOM.disabled = false;
}

// ------------------------
// Calcul et affiche le montant total du panier
// ------------------------
function countCartTotal() {
  let cartTotal = 0;
  let cartQuantity = 0;
  cart.forEach((cartItem) => {
    cartTotal += cartItem.quantity * cartItem.price;
    cartQuantity++;
  });
  cartFooterTotal.innerText = cartTotal;
  document.querySelector(".cart-quantity").innerText = cartQuantity;
  // console.log(cartQuantity);
}

// ------------------------
// Retourne la quantité totale de produits dans le panier
// ------------------------
function countCartTotalQuantity() {
  let cartQuantity = 0;
  cart.forEach((cartItem) => {
    cartQuantity += cartItem.quantity;
  });
  // console.log(cartQuantity);
  return cartQuantity;
}

function saveCart() {
  localStorage.setItem("cart", JSON.stringify(cart));
  countCartTotal();
}

// ------------------------
// Incrémente la quantité d’un produit.
// ------------------------
function increaseItem(product, cartItemDOM) {
  cart.forEach((cartItem) => {
    if (cartItem.name === product.name) {
      cartItemDOM.querySelector(
        '[data-action="DECREASE_ITEM"]'
      ).disabled = false;
      cartItem.quantity += 1;
      cartItemDOM.querySelector(".cart__item__quantity").innerText =
        cartItem.quantity;
      saveCart();
    }
  });
}

function decreaseItem(product, cartItemDOM) {
  cart.forEach((cartItem) => {
    if (cartItem.name === product.name) {
      if (cartItem.quantity > 1) {
        cartItem.quantity -= 1;
        cartItemDOM.querySelector(".cart__item__quantity").innerText =
          cartItem.quantity;
        if (cartItem.quantity === 1) {
          cartItemDOM.querySelector(
            '[data-action="DECREASE_ITEM"]'
          ).disabled = true;
        }
        saveCart();
      }
    }
  });
}

// ------------------------
// Supprime un produit du panier.
// ------------------------
function removeItem(product, cartItemDOM) {
  cart.forEach((cartItem) => {
    if (cartItem.name === product.name) {
      cartItemDOM.classList.add("cart__item--removed");
      setTimeout(() => cartItemDOM.remove(), 250);
      cart = cart.filter((item) => item.name !== product.name);
      saveCart();
    }
  });
  handleAddProductButton(product);
}

// ------------------------
// Initialise les gestionnaires d'évènement pour la gestion des produits du panier
// ------------------------
function handleCartActionButtons() {
  if (cart.length > 0) {
    cart.forEach((cartItem) => {
      const product = cartItem;
      const cartItemsDOM = cartDOM.querySelectorAll(".cart__item");
      cartItemsDOM.forEach((cartItemDOM) => {
        if (
          cartItemDOM.querySelector(".cart__item__name").innerText ===
          product.name
        ) {
          cartItemDOM
            .querySelector('[data-action="INCREASE_ITEM"]')
            .addEventListener("click", () =>
              increaseItem(product, cartItemDOM)
            );
          cartItemDOM
            .querySelector('[data-action="DECREASE_ITEM"]')
            .addEventListener("click", () =>
              decreaseItem(product, cartItemDOM)
            );
          cartItemDOM
            .querySelector('[data-action="REMOVE_ITEM"]')
            .addEventListener("click", () => {
              removeItem(product, cartItemDOM);
              if (cart.length < 1) {
                clearCartButtonDOM.disabled = true;
                payButtonDOM.disabled = true;
              }
            });
        }
      });
    });
  }
}

// ------------------------
// Gère l’état des boutons AJOUTER et PAYER sur les produits.
// ------------------------
function handleAddProductButton(product) {
  const addtoCartBtn = document.querySelector('[data-action="ADD_TO_CART"]');
  const payButton = document.querySelector('[data-action="CHECKOUT_PRODUCT"]');
  const isInCart =
    cart.filter((cartItem) => cartItem.name === product.name).length > 0;
  if (isInCart) {
    addtoCartBtn.innerText = "Dans le Panier";
    addtoCartBtn.disabled = true;
    payButton.disabled = false;
    payButton.addEventListener("click", () => Utils.showCart());
  } else {
    addtoCartBtn.innerText = "Ajouter au panier";
    addtoCartBtn.disabled = false;
    payButton.disabled = true;
  }
}

function clearCart() {
  cartDOM.querySelectorAll(".cart__item").forEach((cartItemDOM) => {
    cartItemDOM.classList.add("cart__item--removed");
    setTimeout(() => cartItemDOM.remove(), 250);
  });
  cart = [];
  localStorage.removeItem("cart");
  clearCartButtonDOM.disabled = true;
  payButtonDOM.disabled = true;
  addToCartButtonsDOM.forEach((addToCartButtonDOM) => {
    addToCartButtonDOM.innerText = "Ajouter au Panier";
    addToCartButtonDOM.disabled = false;
  });
  countCartTotal();
  // window.location.href = "/#/";
}

function goCheckoutForm() {
  window.location.href = "/#/checkout";
  Utils.closeCart();
}

function getCart() {
  if (cart.length > 0) {
    return cart;
  }
}

// ------------------------
// Récupère la liste des ID de produits depuis le panier( s’il y en a)
// ------------------------
function getCartId() {
  let productsId = [];
  if (cart.length > 0) {
    cart.forEach((cartItem) => {
      productsId.push(cartItem._id);
    });
  }
  return productsId;
}

// ------------------------
// Génère la page de résultat de la commande de produits à partir des données renvoyées par le serveur.
// ------------------------
var orderResponse = function (data) {
  const contact = data.contact;
  let cart = JSON.parse(localStorage.getItem("cart")) || [];
  const productQuantity = cart.length;
  const prixTotal = document.querySelector(".cart-footer__total").innerText;
  const quantiteDeProduit = countCartTotalQuantity();

  const resOrder = /*html*/ ` 
    <section class="section flex lg">
        <h2 class="section__title">Votre commande est enregistrée !</h2>
        <div class="confirm">
          <p>
            Commande n°: ${data.orderId}
            <br>
            Nombre de modèles: ${productQuantity}
            <br>
            Nombre de produits: ${quantiteDeProduit}
            <br>
            Prix total: ${prixTotal} €
          </p>
          <div class="contact">
            <ul id="cmd-${data.orderId}">
                <li>Nom: ${data.contact.firstName}</li>
                <li>Prénom: ${contact.lastName}</li>
                <li>Addresse: ${contact.address}</li>
                <li>Ville: ${contact.city}</li>
                <li>Email: ${contact.email}</li>
            </ul>
            <a href="/#/"><div class="btn btn--primary btn--contact">Retourner à l'accueil</div></a>
          </div>
        </div>
        <div class="cart">
            ${data.products
              .map((product) => {
                let cartItem = cart.filter((item) => item._id === product._id);

                return /*html*/ `
                    <div class="cart__item cart-confirm">
                        <img class="cart__item__image" src="${product.imageUrl}" alt="${product.name}">
                        <h3 class="cart__item__name">${product.name}</h3>
                        <h3>Qt:&nbsp;${cartItem[0].quantity}</h3>
                        <h3 class="cart__item__price">${product.price}</h3>
                        <span>ref: &nbsp; ${product._id}</span>
                    </div>`;
              })
              .join("\n ")}
        </div>
        
    </section>`;
  return resOrder;
};

// ------------------------
// Génère la requête de commande de produits à partir des données du formulaire de contact et de la liste des Id’s de produits.
// ------------------------
function checkout(e) {
  e.preventDefault();

  let ckeckoutForm = document.getElementById("formCheckout");
  const firstname = ckeckoutForm.elements["firstName"].value;
  const lastname = ckeckoutForm.elements["lastName"].value;
  const address = ckeckoutForm.elements["address"].value;
  const city = ckeckoutForm.elements["city"].value;
  const email = ckeckoutForm.elements["email"].value;

  let data = {
    products: getCartId(),
    contact: {
      firstName: firstname,
      lastName: lastname,
      address: address,
      city: city,
      email: email,
    },
  };

  let submitCheckout = async () => {
    try {
      // const response = await fetch("http://localhost:3000/api/allproducts", {
      const response = await fetch("http://localhost:3000/api/teddies/order", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(data),
      });
      const json = await response.json();
      return json;
    } catch (err) {
      console.log("Error getting documents", err);
    }
  };

  let serverResponse = {
    render: async () => {
      const data = await submitCheckout();
      const content = null || document.getElementById("page_container");
      content.innerHTML = orderResponse(data);
    },
  };

  serverResponse.render();
  setTimeout(clearCart, 3000);
}

export {
  initCart,
  initProduit,
  handleAddProductButton,
  handleCartActionButtons,
  checkout,
  getCart,
  getCartId,
};
